# nmconv

[![Test Coverage](https://img.shields.io/badge/coverage-48%25-red.svg)](file:coverage.html)
[![Go Report Card](https://goreportcard.com/badge/codeberg.org/fractalqb/nmconv)](https://goreportcard.com/report/codeberg.org/fractalqb/nmconv)
[![GoDoc](https://godoc.org/codeberg.org/fractalqb/nmconv?status.svg)](https://pkg.go.dev/git.fractalqb.de/fractalqb/nmconv)

`import "git.fractalqb.de/fractalqb/nmconv"`

---

Convert identifiers between different naming conventions

When one wants to convert names between e.g. camel-, snake- or
lisp-case this nmconvage might come in handy.
