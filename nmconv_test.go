package nmconv

import (
	"fmt"
	"reflect"
	"strings"
	"testing"

	"github.com/stvp/assert"
)

type uncameltst struct {
	Input  string
	Expect []string
}

func (ut uncameltst) run(t *testing.T) {
	res := Uncamel(ut.Input)
	if !reflect.DeepEqual(res, ut.Expect) {
		t.Errorf("from '%s' expect %s, got %s",
			ut.Input,
			ut.Expect,
			res)
	}
}

func uncamelTest(in string, expect ...string) uncameltst {
	return uncameltst{in, expect}
}

func TestUncamel(t *testing.T) {
	t.Run("1wordLow", uncamelTest("foo", "foo").run)
	t.Run("1wordUp", uncamelTest("Foo", "Foo").run)
	t.Run("2wordLow", uncamelTest("fooBar", "foo", "Bar").run)
	t.Run("2wordUp", uncamelTest("FooBar", "Foo", "Bar").run)
	t.Run("short", uncamelTest("FB", "F", "B").run)
	t.Run("bmcLow", uncamelTest("äüÖü", "äü", "Öü").run)
	t.Run("bmcUp", uncamelTest("ÄüÖü", "Äü", "Öü").run)
}

func TestCamelLow(t *testing.T) {
	res := Camel1Low([]string{"FOO", "bar"})
	assert.Equal(t, "fooBar", res)
}

func TestCamelUp(t *testing.T) {
	res := Camel1Up([]string{"foo", "BAR"})
	assert.Equal(t, "FooBar", res)
}

func TestCamel_shortLow(t *testing.T) {
	res := Camel1Low([]string{"f", "b"})
	assert.Equal(t, "fB", res)
}

func TestCamel_shortUp(t *testing.T) {
	res := Camel1Up([]string{"f", "b"})
	assert.Equal(t, "FB", res)
}

func TestCapWord(t *testing.T) {
	capTest := func(toCap, result string) {
		if cap := CapWord(toCap); cap != result {
			t.Errorf("expect '%s' from '%s', got '%s'", result, toCap, cap)
		}
	}
	capTest("joHn", "John")
	capTest("ökonom", "Ökonom")
}

func TestFunConv(t *testing.T) {
	conv := Conversion{Norm: Unsep("_"), Denorm: Sep("-")}
	res := conv.Convert("foo_bar_baz")
	if res != "foo-bar-baz" {
		t.Error(res)
	}
}

func BenchmarkFunConv(b *testing.B) {
	conv := Conversion{Norm: Unsep("_"), Denorm: Sep("-")}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		conv.Convert("foo_bar_baz")
	}
}

func ExamplePassConversion() {
	user := func(nm string, conv func(string) string) {
		fmt.Printf("'%s' → '%s'", nm, conv(nm))
	}
	conv := Conversion{Norm: Unsep("_"), Denorm: Sep("-")}
	user("foo_bar_baz", conv.Convert)
	// Output:
	// 'foo_bar_baz' → 'foo-bar-baz'
}

type IfConvn interface {
	Norm(str string) []string
	Denorm(n []string) string
}

type IfSep string

func (s IfSep) Norm(str string) []string {
	return strings.Split(str, string(s))
}

func (s IfSep) Denorm(n []string) string {
	return strings.Join(n, string(s))
}

type IfCnv struct {
	From IfConvn
	To   IfConvn
}

func (cnv IfCnv) Convert(str string) string {
	tmp := cnv.From.Norm(str)
	return cnv.To.Denorm(tmp)
}

func BenchmarkIfConv(b *testing.B) {
	conv := IfCnv{From: IfSep("_"), To: IfSep("-")}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		conv.Convert("foo_bar_baz")
	}
}

// TODO new tests for the Transform concept
